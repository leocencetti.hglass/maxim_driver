#include "rpc_mock.h"

bool parse_RPC_command(const std::string &str, RPC_msg *output) {
  std::regex regex("/(\\w+)/(\\w+) ?([\\d.]+)?");
  std::smatch match;

  bool ret = std::regex_match(str, match, regex);
  if (ret) {
    output->variable = match[1].str();

    output->operation = str2action(match[2].str());
    if (match.size() > 3) {
      output->data = match[3].str();
    }
  } else {
    printf("No matching command\n");
  }
  return ret;
}

RPC_Action str2action(const std::string &str){
    if (str == "set"){
        return RPC_Action::set;
    }
      if (str == "get"){
        return RPC_Action::get;
    }
    return RPC_Action::unknown;
}