#include "common_utils.h"

const char* bit_rep[16] = {
    "0000", "0001", "0010", "0011", "0100", "0101", "0110", "0111",
    "1000", "1001", "1010", "1011", "1100", "1101", "1110", "1111",
};

char* byte_print_buffer[8] = {};

std::string b2s(uint8_t byte) {
  char str[8];
  sprintf(str, "%s%s", bit_rep[byte >> 4], bit_rep[byte & 0x0F]);
  return std::string(str);
}