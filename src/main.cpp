#include "main.h"
#include <cstdio>

int main() {
  printf("\nI2C ETL Test\n");
  i2c.frequency(100000);

  u8 res;

  if (!init()) {
    printf("Could not connect to I2C device\n");
    return -1;
  }

  u8 buffer[12] = {0};

  // Put the device in Active mode
  res = setActiveMode(buffer);

  // Set internal temp sensor
  buffer[0] = EXTERNAL_TEMPSENS;
  res = registryWrite(DRIVERCONF_REG_ADDR, buffer, 1);

  float VoltageOutput = 45.0;

  RPC_msg rpc_msg;

  res = setLensVoltage(VoltageOutput, buffer);

  char *ch = new char[1];
  std::string command = "";
  while (1) {
    if (pc.readable()) {
      pc.read(ch, sizeof(ch));
      pc.write(ch, sizeof(ch));
      if (ch[0] != '\n' && ch[0] != '\r') {
        command += ch;
      } else {
        printf("Running command: %s\n", command.c_str());
        if (parse_RPC_command(command, &rpc_msg)) {

          if (rpc_msg.variable == std::string("voltage")) {
            switch (rpc_msg.operation) {
            case RPC_Action::set:
              VoltageOutput = std::stof(rpc_msg.data);
              setLensVoltage(VoltageOutput, buffer);
              break;
            case RPC_Action::get:
              printf("Voltage: %f\n", VoltageOutput);
              break;
            default:
              printf("Unknown operation.");
            }
          }
        }
        command = "";
      }
    }
    // printStatus(buffer);
    // printTemperature(buffer);
    // printErrorStatus(buffer);
    // printUserMode(buffer);
    // printDriverConf(buffer);
  }
}
