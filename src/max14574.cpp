#include "max14574.h"
// DigitalIn sdaDummy(I2C_SDA, PullUp);
// DigitalIn sclDummy(I2C_SCL, PullUp);
I2C i2c(I2C_SDA, I2C_SCL);

bool init() {
  // Check that the device is available
  i2c.start();
  u8 res = i2c.write(MAX14574_WRITE_ADDR);
  i2c.stop();
  return res == 1;
}

void printOK(u8 res) {
  if (res == I2C_WRITE_OK) {
    printf("OK\n");
  } else {
    printf("ERROR\n");
  }
}

void printErrorStatus(u8 *buffer) {
  printf(">>> Updating failure registry... ");
  buffer[0] = CHECK_FAILURE;
  printOK(registryWrite(COMMAND_REG_ADDR, buffer, 1));
  ThisThread::sleep_for(1ms);
  buffer[0] = 0x22;
  registryRead(FAIL_REG_ADDR, buffer);
  printf("<<< Failure byte: %s\n", b2s(buffer[0]).c_str());
}

void printStatus(u8 *buffer) {
  printf(">>> Reading status registry... ");
  printOK(registryRead(STATUS_REG_ADDR, buffer));
  printf("<<< Status byte: %s\n", b2s(buffer[0]).c_str());
}

void printUserMode(u8 *buffer) {
  printf(">>> Reading UserMode registry... ");
  printOK(registryRead(USERMODE_REG_ADDR, buffer));
  printf("<<< UserMode byte: %s\n", b2s(buffer[0]).c_str());
}

void printTemperature(u8 *buffer) {
  printf(">>> Reading temperature registry... ");
  printOK(registryRead(TEMPSENSE_REG_ADDR, buffer));
  printf("<<< Temperature byte: %s\n", b2s(buffer[0]).c_str());
}

void printDriverConf(u8 *buffer) {
  printf(">>> Reading DriverConf registry... ");
  printOK(registryRead(DRIVERCONF_REG_ADDR, buffer));
  printf("<<< DriverConf byte: %s\n", b2s(buffer[0]).c_str());
}

void printLensVoltage(u8 *buffer) {
  printf(">>> Reading voltage registry... ");
  registryRead(LLV1_REG_ADDR, buffer);
  u16 voltage = buffer[0];
  printOK(registryRead(OISLSB_REG_ADDR, buffer));
  voltage <<= 2;
  voltage = voltage | (u16)(buffer[0] & 0x03);
  printf("<<< Voltage bytes: %s %s\n", b2s(voltage >> 8).c_str(),
         b2s(voltage & 0xFF).c_str());
}

u8 setLensVoltage(float voltage, u8 *buffer) {
  printf("--- Setting lens voltage to %g V... ", voltage);
  u16 vcode = computeVoltageCode(voltage);
  u8 temp = vcode & 0x03;
  u8 res;

  buffer[0] = temp | temp << 2 | temp << 4 | temp << 6;
  buffer[1] = buffer[2] = buffer[3] = buffer[4] = vcode >> 2;
  buffer[5] = UPDATE_V_OUTPUT;
  printOK(res = registryWrite(OISLSB_REG_ADDR, buffer, 6));
  return res;
}

u16 computeVoltageCode(float voltage) {
  if (voltage < LENS_V_MIN) {
    voltage = LENS_V_MAX;
  }
  if (voltage > LENS_V_MAX) {
    voltage = LENS_V_MIN;
  }

  u16 code = (u16)(DRIVER_V_CODE_MAX - DRIVER_V_CODE_MIN) /
                 (DRIVER_V_MAX - DRIVER_V_MIN) * (voltage - DRIVER_V_MIN) +
             DRIVER_V_CODE_MIN;
  return code;
}

u8 setActiveMode(u8 *buffer) {
  printf("Setting Active Mode...");
  buffer[0] = USERMODE_ACTIVE;
  u8 res = registryWrite(USERMODE_REG_ADDR, buffer, 1);
  printOK(res);
  return res;
}

u8 setSleepMode(u8 *buffer) {
  printf("Setting Sleep Mode...");
  buffer[0] = USERMODE_SLEEP;
  u8 res = registryWrite(USERMODE_REG_ADDR, buffer, 1);
  printOK(res);
  return res;
}

u8 registryRead(u8 regAddr, u8 *buffer) {
  u8 res = 0;
  i2c.lock();
  i2c.start();
  res = i2c.write(MAX14574_WRITE_ADDR);
  res = i2c.write(regAddr);
  i2c.start();
  res = i2c.write(MAX14574_READ_ADDR);
  buffer[0] = i2c.read(0);
  i2c.stop();
  i2c.unlock();
  return res;
}

u8 registryWrite(u8 regAddr, u8 *buffer, u8 size) {
  u8 res = 0;
  i2c.lock();
  i2c.start();
  res = i2c.write(MAX14574_WRITE_ADDR);
  res = i2c.write(regAddr);
  for (int i = 0; i < size; i++) {
    res = i2c.write(buffer[i]);
  }
  i2c.stop();
  i2c.unlock();
  return res;
}