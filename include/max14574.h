#ifndef _MAX14574_H
#define _MAX14574_H
#include "mbed.h"
#include "common_utils.h"

typedef uint8_t u8;
typedef uint16_t u16;

const u8 MAX14574_WRITE_ADDR = 0xEE;
const u8 MAX14574_READ_ADDR = 0xEF;

const u8 STATUS_REG_ADDR = 0x00;
const u8 FAIL_REG_ADDR = 0x01;
const u8 TEMPSENSE_REG_ADDR = 0x02;
const u8 USERMODE_REG_ADDR = 0x03;
const u8 OISLSB_REG_ADDR = 0x04;
const u8 LLV1_REG_ADDR = 0x05;
const u8 LLV2_REG_ADDR = 0x06;
const u8 LLV3_REG_ADDR = 0x07;
const u8 LLV4_REG_ADDR = 0x08;
const u8 COMMAND_REG_ADDR = 0x09;
const u8 DRIVERCONF_REG_ADDR = 0x0A;

const u8 USERMODE_SLEEP = 0x00;
const u8 USERMODE_ACTIVE = 0x03;
const u8 INTERNAL_TEMPSENS = 0x80;
const u8 EXTERNAL_TEMPSENS = 0x40;
const u8 UPDATE_V_OUTPUT = 0x02;
const u8 CHECK_FAILURE = 0x01;

const u8 I2C_WRITE_OK = 0x01;

const float LENS_V_MIN = 31.0;
const float LENS_V_MAX = 64.0;

const float DRIVER_V_MIN = 24.4;
const float DRIVER_V_MAX = 69.7;
const u16 DRIVER_V_CODE_MIN = 0x000;
const u16 DRIVER_V_CODE_MAX = 0x3FF;


extern I2C i2c;

u8 registryRead(u8 regAddr, u8 *buffer);
u8 registryWrite(u8 regAddr, u8 *buffer, u8 size);
u8 setActiveMode(u8 *buffer);
u8 setSleepMode(u8 *buffer);
void printOK(u8 res);
bool init();
u16 computeVoltageCode(float voltage);
u8 setLensVoltage(float voltage, u8 *buffer);

void printLensVoltage(u8 *buffer);
void printErrorStatus(u8 *buffer);
void printStatus(u8 *buffer);
void printTemperature(u8 *buffer);
void printUserMode(u8 *buffer);
void printDriverConf(u8 *buffer);

#endif