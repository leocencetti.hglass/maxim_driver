#ifndef _RPC_MOCK_H
#define _RPC_MOCK_H
#include <regex>
#include <string>

enum RPC_Action { set, get, unknown };

struct RPC_msg {
  std::string variable;
  RPC_Action operation;
  std::string data;
};


bool parse_RPC_command(const std::string &str, RPC_msg *output);
RPC_Action str2action(const std::string &str);
#endif