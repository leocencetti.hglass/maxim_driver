#ifndef _UTILS_H
#define _UTILS_H
#include <string>

extern const char *bit_rep[16];

extern char* byte_print_buffer[8];

std::string b2s(uint8_t byte);

#endif